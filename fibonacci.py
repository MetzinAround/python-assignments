number = input("How many numbers do you want?")

number = int(number)

if number <= 0:
    print("Please input a positive number!")
    exit()

sequence_number_old = 0
sequence_number_new = 1

print(sequence_number_old)

if number > 1:
    print(sequence_number_new)
    for i in range(2, number):
        new_number = sequence_number_old + sequence_number_new
        sequence_number_old = sequence_number_new
        sequence_number_new = new_number
        print(sequence_number_new)

